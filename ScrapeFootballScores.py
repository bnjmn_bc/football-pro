import dataset
from bs4 import BeautifulSoup
import requests
import time


# ----------------------
# SETUP  
# ----------------------

SITE = "http://www.pro-football-reference.com"

db = dataset.connect('sqlite:///data/football.db')
seasons = db['season']
boxscores = db['boxscore']

if len(db['season']) == 0:
    # initialize season table
    """
    Need to initialize the season table for the first run.
    Otherwise, database lookups will error.
    Years avaible on SITE: 1940 - 2013
    """
    for year in range(1940,2014):
        seasons.insert({'year': year, 'gamesParsed': False})

    
# ----------------------
# Methods for Scraping
# ----------------------

def getYearGames(year):
    """
    This method actually goes and gets the data for the given season
    """
    r = requests.get(SITE + "/years/%s/games.htm" % str(year))
    games = BeautifulSoup(r.text).find(id="games")
    return games


def parseGames(games, year):
    """
    Given the html games table (in element)
    Create a dict of records for each game
    and insert it to the database.
    TODO: change this to upsert to handle existing games
    """
    header = [th.get('data-stat') for th in games.find('thead').find_all('th')]
    body = games.find('tbody')
    for row in body.find_all('tr', class_=""):
        data = [ td.string for td in row.find_all('td')]
        if 'boxscore' in data:
            """Must check for 'boxscore' because there are several header lines throughout
            the data that must be skipped. 'boxscore' only exists for actual games"""
            game = {"year": year}
            game['boxscore_url'] = [ a.get('href') for a in row.find_all('a') 
                    if a.string == 'boxscore'][0]
            # Check if we already have this game in the DB
            if boxscores.find_one(boxscore_url=game['boxscore_url']) is None:
                print "New game found %s" % game['boxscore_url']
                game['quartersParsed'] = False
                game.update(dict(zip(header, data)))
                boxscores.insert(game)
            else:
                print "Game %s already in DB. Not re-inserting." % game['boxscore_url']


def getQuarterScores(linescore):
    """
    Get score for each quater from linescrore
    table on the boxscore page
    """
    def parseTeam(tr, teamStr, head):
        values = [td.string for td in tr.find_all('td')]
        data = {teamStr: tr.find('a').string}
        data.update(dict(zip(
            [h+'_'+teamStr for h in head[1:]], values[1:] )))
        return data

    # Get header, need this in case of OT
    header = [ 'q%s' % th.string for th in linescore.find_all('th')]
    # There is an empty 'tr', skip and parse visiting team
    vTeam = parseTeam(linescore.find_all('tr')[1], 'vTeam', header)
    hTeam = parseTeam(linescore.find_all('tr')[2], 'hTeam', header)
    return dict(vTeam.items() + hTeam.items())

def write_boxscores(filename="data/football.csv"):
    dataset.freeze(db['boxscore'].all(), format='csv',
            filename=filename)

def main():
    #years = range(1940, 2014) # Possible range
    years = range(2005, 2014) # Reasonable range
    for year in reversed(years):
        if not seasons.find_one(year=year)['gamesParsed'] == True:
            print "Parsing %s Season" % str(year)
            parseGames(getYearGames(year), year)
            seasons.update({'year': year,'gamesParsed': True}, ['year'])
        else:
            print "%s Season already parsed" % str(year)
    print "Initial parsing of requested games(season) complete"

    # Hack. Feature request. Update current season games each week.
    print "Update current season"
    parseGames(getYearGames(2013), 2013)
    print "Current season game list update complete"

    # Get data(score) for each quarter
    print "Now scraping for Quarter scores. Careful w/ resources"
    for game in list(db['boxscore']):
        if not game['quartersParsed'] == True:
            time.sleep(2)
            url = game['boxscore_url']
            print "Getting and parsing quarter scores for %s" % str(url)
            r = requests.get(SITE + url)
            linescore = BeautifulSoup(r.text).find(id="linescore")
            data = getQuarterScores(linescore)
            game.update(data)
            game['quartersParsed'] = True
            boxscores.update(game, ['id'])
        else:
            print "Game %s already parsed" % str(game['boxscore_url'])
    print "All parsing complete"
    print "Writing table to csv"
    write_boxscores()

# ----------------------
# Start Scraping 
# ----------------------

if __name__ == "__main__":
    main()
