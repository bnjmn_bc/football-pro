import dataset

# Alright, this is just db setup
# somewhere to store stuff
db = dataset.connect('sqlite:///football.db')

# Now inspecting the site.
# A list of all available years
# http://www.pro-football-reference.com/boxscores/
# 2013 - 1940, should be enough

# Each season is all on one page
# http://www.pro-football-reference.com/years/2013/games.htm
# pretty simple to parse so far.

# Now each boxscore is actually behind a somewhat more
# difficult to intuit URL. EXAMPLE
# http://www.pro-football-reference.com/boxscores/201309080nor.htm
# Basically, /boxscores/YYYYMMDD<home_team_abbr>.htm
# I don't know who played when.
# Let's scrape it.
from bs4 import BeautifulSoup
import requests
from lxml import etree

year = 2013
SITE = "http://www.pro-football-reference.com"

url = SITE + "/years/%s/games.htm" % str(year)
r = requests.get(url)
soup = BeautifulSoup(r.text)

seasons = db['season']
boxscores = db['boxscore']
# Just get the table with the data
games = soup.find(id="games")
for link in games.find_all('a')[0:2]:
    if link.string == "boxscore":
        #boxscores.insert(dict(url=link.get('href'), season=year))
        print link.get('href')
        r = requests.get(SITE + link.get('href'))
        box = BeautifulSoup(r.text)
        linescore = box.find(id="linescore")
        headers = [ 'q%s' % th.string for th in linescore.find_all('th')]
        headers[0] = "team"
        for row in linescore.find_all('tr')[1:]:
            values = [ td.string for td in row.find_all('td')]
            values[0] = row.find('a').string
            print dict(zip(headers, values))


